////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////


package com.saasplications.genie.domain.model

import org.jetbrains.spek.api.Spek
import kotlin.test.assertEquals

class MessageTypeSpec : Spek({

    it("should deserialize from string, ignoring case") {
        assertEquals(MessageType.WARNING, MessageType.fromString("wArNinG"))
        assertEquals(MessageType.ERROR, MessageType.fromString("ERROR"))
    }
})