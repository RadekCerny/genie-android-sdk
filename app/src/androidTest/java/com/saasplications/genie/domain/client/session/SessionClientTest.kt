////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package com.saasplications.genie.domain.client.session

import android.app.Application
import android.test.ApplicationTestCase
import com.saasplications.genie.configuraiton.UserType
import com.saasplications.genie.domain.client.ResponseStatus
import com.saasplications.genie.domain.client.login.LoginClientDefaultImpl
import com.saasplications.genie.domain.client.login.payload.SessionRequest
import com.saasplications.genie.domain.client.session.payload.SessionDataRequest
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

class SessionClientTest : ApplicationTestCase<Application>(Application::class.java)
{
    var sessionToken: String? = null

    fun testLoadSessionData()
    {
        val loginSignal = CountDownLatch(1)
        val loginUrl = "http://crp.saasplications.com/GenieService/genieservice.svc/restish/CreateSessionX"
        val loginClient = LoginClientDefaultImpl(loginUrl)
        val sessionRequest = SessionRequest(userName = "jasper", password = "sdk",
                appSite = "SOULFRESHTEST", userType = UserType.Primary)
        loginClient.createSession(sessionRequest) {
            assertEquals(ResponseStatus.SUCCESS, it.status)
            assertNotNull(it.result)
            this.sessionToken = it.result!!.sessionToken
            loginSignal.countDown()
        }

        loginSignal.await(7, TimeUnit.SECONDS)


        val sessionDataSignal = CountDownLatch(1)

        val sessionDataUrl = "http://crp.saasplications.com/GenieService/genieservice.svc/restish/ExecX"
        val sessionClient = SessionDataClientDefaultImpl(sessionDataUrl)
        val request = SessionDataRequest(this.sessionToken!!)

        sessionClient.loadSessionData(request) {
            assertEquals(ResponseStatus.SUCCESS, it.status)
            println("################################ session data" + it)
            sessionDataSignal.countDown()
        }
        sessionDataSignal.await(7, TimeUnit.SECONDS)

    }
}




