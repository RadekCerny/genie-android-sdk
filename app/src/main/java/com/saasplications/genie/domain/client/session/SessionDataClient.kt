////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////


package com.saasplications.genie.domain.client.session

import com.saasplications.genie.domain.client.Response
import com.saasplications.genie.domain.client.session.payload.SessionDataRequest
import com.saasplications.genie.domain.model.SessionData

interface SessionDataClient
{
    fun loadSessionData(sessionDataRequest: SessionDataRequest,
                        completion: (Response<SessionData>) -> Unit)
}