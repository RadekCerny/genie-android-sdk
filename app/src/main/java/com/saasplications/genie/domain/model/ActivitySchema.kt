////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package com.saasplications.genie.domain.model

import java.util.*

class ActivitySchema(val activityId: String, val title: String)
{
    val fields: ArrayList<FieldSchema>
    val methods: ArrayList<MethodSchema>
    val styles: ArrayList<String>
    val queries: ArrayList<Query>

    //---------------------------------------------------------------------------------------
    //MARK: - Constructors
    //---------------------------------------------------------------------------------------

    init
    {
        this.fields = ArrayList<FieldSchema>()
        this.methods = ArrayList<MethodSchema>()
        this.styles = ArrayList<String>()
        this.queries = ArrayList<Query>()

    }

    fun displayableFields(): List<FieldSchema>
    {
        return fields.filter { isDisplayable(it) }
    }

    fun addField(field: FieldSchema)
    {
        this.fields.add(field)
    }

    fun addMethod(method: MethodSchema)
    {
        this.methods.add(method)
    }

    fun addStyle(style: String)
    {
        this.styles.add(style)
    }

    fun addQuery(query: Query)
    {
        this.queries.add(query)
    }

    //---------------------------------------------------------------------------------------
    //MARK: - Overridden Methods
    //---------------------------------------------------------------------------------------

    override fun toString(): String
    {
        return "ActivitySchema: fields=$fields, methods=$methods, styles=$styles, queries=$queries"
    }

    //---------------------------------------------------------------------------------------
    //MARK: - Private Methods
    //---------------------------------------------------------------------------------------

    fun isDisplayable(field: FieldSchema): Boolean
    {
        return !(field.name == "PersistentId" || field.name == "RowVersion" || field.name == "RowClassType")
    }

}