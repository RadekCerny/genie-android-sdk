////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////


package com.saasplications.genie.domain.client.login


import com.saasplications.genie.domain.client.Response
import com.saasplications.genie.domain.client.login.payload.SessionRequest
import com.saasplications.genie.domain.model.SessionContext

interface LoginClient
{

    fun createSession(sessionRequest: SessionRequest,
                      completion: (Response<SessionContext>) -> Unit)

}