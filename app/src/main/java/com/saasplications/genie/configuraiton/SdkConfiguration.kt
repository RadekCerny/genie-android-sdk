////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package com.saasplications.genie.configuraiton

import com.saasplications.genie.utils.appendPathComponent
import java.net.URL

class SdkConfiguration(var baseUrl: URL?, var preferredSite: String?, var userType: UserType?) {

    fun createSessionServiceUrl(): URL {
        return baseUrl!!.appendPathComponent("CreateSessionX")
    }

    fun execXServiceUrl(): URL {
        return baseUrl!!.appendPathComponent("ExecX")
    }

    val blobServiceUrl: URL
        get() = baseUrl!!.appendPathComponent("GetBlob")

    fun listAvailableSitesUrl(): URL {
        return baseUrl!!.appendPathComponent("ListAvailableSitesX")
    }

    fun listActivitiesForSiteUrl(): URL {
        return baseUrl!!.appendPathComponent("ListActivitiesForSiteX")
    }

    fun schemaForActivityUrl(): URL {
        return baseUrl!!.appendPathComponent("GetSchemaForActivityX")
    }

    override fun toString(): String {
        return String.format("SDKConfiguration: url=%s, preferredSite=%s, userType=%s", baseUrl!!.toString(), preferredSite, userType)
    }


}