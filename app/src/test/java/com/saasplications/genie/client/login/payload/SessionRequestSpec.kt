////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////


package com.saasplications.genie.client.login.payload

import com.saasplications.genie.configuraiton.UserType
import com.saasplications.genie.domain.client.login.payload.SessionRequest
import com.saasplications.genie.utils.ResourceAsString
import org.jetbrains.spek.api.Spek
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

class SessionRequestSpec : Spek({

    describe("Object creation", {

        it("Should allow initialization with ", {
            val request = SessionRequest(userName = "jasper", password = "sdk", appSite = "SOULFRESH", userType = UserType.Primary)
            assertNotNull(request)
        })
    })

    describe("Serialization", {

        it("Should serialize itself to XML", {
            val request = SessionRequest(userName = "jasper", password = "sdk", appSite = "SOULFRESH", userType = UserType.Primary)
            val xml = request.toXml()
            assertEquals(ResourceAsString("SessionRequest.xml"), xml)
        })
    })

})
