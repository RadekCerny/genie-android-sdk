////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package com.saasplications.genie.domain.model

import org.jetbrains.spek.api.Spek
import kotlin.test.assertEquals
import kotlin.test.assertNotNull


class TextCellSpec : Spek({

    var cell: TextGridDataCell? = null

    beforeEach {
        cell = TextGridDataCell(cellId = "firstName", text = "Jasper")
    }

    describe("Object creation") {

        it("should allow initialization with cellId and data attributes.") {
            assertNotNull(cell)
            assertEquals("firstName", cell!!.cellId)
            assertEquals("Jasper", cell!!.text)
        }
    }

})