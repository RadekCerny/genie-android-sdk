////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package com.saasplications.genie.configuraiton


enum class UserType
{
    Primary,
    Alternate;

    companion object
    {
        fun fromString(string: String): UserType
        {
            return when (string)
            {
                "Primary" -> Primary
                "Alternate" -> Alternate
                else -> Primary
            }
        }
    }

    override fun toString(): String
    {
        return when (this)
        {
            Primary -> "Primary"
            Alternate -> "Alternate"
        }
    }
}
