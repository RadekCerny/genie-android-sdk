////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package com.saasplications.genie.domain.model

import java.util.ArrayList

class GridData(dataId: String,
               val source: String,
               var query: String?,
               var contextObject: String?) : AbstractData(dataId)
{

    val columns: ArrayList<Column>
    val rows: MutableList<Row>
        get() {
            field.sortBy { it.id }
            return field
        }

    //---------------------------------------------------------------------------------------
    //MARK: - Constructors
    //---------------------------------------------------------------------------------------

    init
    {
        this.columns = ArrayList<Column>()
        this.rows = ArrayList<Row>()

    }

    fun fieldIdentifiers(): List<Any>
    {
        val allFieldNames = ArrayList<String>()
        for (column in columns)
        {
            allFieldNames.add(column.fieldId)
        }
        return allFieldNames.sorted()
    }

    fun addColumn(column: Column)
    {
        column.dataSet = this
        columns.add(column)
    }

    fun columnWithId(columnId: String): Column?
    {
        return columns.find { it.id == columnId }
    }

    fun columnWithFieldId(fieldId: String): Column?
    {
        return columns.find { it.fieldId == fieldId }
    }

    fun addRow(row: Row)
    {
        row.gridData = this
        rows.add(row)
    }

    fun rowWithId(rowId: String): Row?
    {
        return rows.find { it.id == rowId }
    }

    override fun count(): Int
    {
        return rows.size
    }

    //---------------------------------------------------------------------------------------
    //MARK: - Overridden Methods
    //---------------------------------------------------------------------------------------

    override fun toString(): String
    {
        return "GridData id=$dataId"
    }


}