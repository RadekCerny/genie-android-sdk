////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////


package com.saasplications.genie.client.login.payload

import com.saasplications.genie.domain.client.ServiceException
import com.saasplications.genie.domain.client.login.payload.toSessionContext
import com.saasplications.genie.utils.RXMLElement
import com.saasplications.genie.utils.ResourceAsString
import org.jetbrains.spek.api.Spek
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertNull
import kotlin.test.fail

class RXMLElement_SessionContextSpec : Spek({

    describe("Valid XML", {

        it("should return a SessionContext holder instance, which holds the session token", {
            val xml = ResourceAsString("SessionRequestSuccess.xml")
            val element = RXMLElement.elementFromXMLString(xml)

            val sessionContext = element.toSessionContext()
            assertNotNull(sessionContext.sessionToken)
            assertNull(sessionContext.message)
        })
    })


    describe("Valid XML with a warning", {
        it("Should return a SessionContext with warning message set.", {
            val xml = ResourceAsString("SessionRequestWarning.xml")
            val element = RXMLElement.elementFromXMLString(xml)
            val sessionContext = element.toSessionContext()
            assertNotNull(sessionContext.sessionToken)
            assertNotNull(sessionContext.message)
        })
    })


    describe("Valid XML with an error", {

        it("should return a SessionContext with the error flag set.", {
            val xml = ResourceAsString("SessionRequestError.xml")
            val element = RXMLElement.elementFromXMLString(xml)
            try
            {
                val sessionContext = element.toSessionContext()
                fail("Should have thrown exception")
            } catch (e: ServiceException)
            {
                assertEquals("Unknown User", e.message)
            }

        })
    })


    describe("Attempting to instantiate a SessionContext from invalid XML", {

        it("should throw NSException if the xml root element is not CreateSessionXResponse", {
            try
            {
                val element = RXMLElement.elementFromXMLString("<xml><foobar/></xml>")
                element.toSessionContext()
                fail("should have thrown exception")
            } catch (e: Exception)
            {
                assertEquals("Element is not a CreateSessionXResponse.", e.message)
            }
        })

        it("should return an error if response contains neither a token nor error message.", {
            val element = RXMLElement.elementFromXMLString("<CreateSessionXResponse/>")
            try {
                element.toSessionContext()
                fail("Should have thrown exception")
            }
            catch (e: Exception) {
                println(e.message)
            }
        })

    })

})