////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package com.saasplications.genie.domain.model

import org.jetbrains.spek.api.Spek
import kotlin.test.assertEquals
import kotlin.test.assertNotNull


class ImageGridDataSpec : Spek({


    describe("Object creation") {
        it("should allow creation with cellId and imageUrl attributes") {
            val imageCell = ImageGridDataCell(cellId = "mugShot", path = "/jblues")
            assertNotNull(imageCell)
            assertEquals("mugShot", imageCell.cellId)
            assertEquals("/jblues", imageCell.path)
        }
    }

//    describe("Loading images", {
//
//        it("should load the image in an asynchronous thread", {
//            val url = "/imaes/srpr/logo3w.png"
//            val imageCell = ImageGridDataCell(cellId = "mugShot", path = url)
//            var data: ByteArray? = null
//            imageCell.loadWithBaseUrl("https://www.google.com.ph") {
//                data = it
//            }
//            waitFor(seconds = 7, condition = {
//                data != null
//            }, thenPerformTests = {
//
//            })
//        })
//    })

})
