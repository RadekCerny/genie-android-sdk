////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package com.saasplications.genie.domain.model


import java.util.ArrayList

class TreeData(dataId: String) : AbstractData(dataId) {

    val folders: ArrayList<Folder>
        get() {
            field.sortBy { it.sequence }
            return field
        }

    //---------------------------------------------------------------------------------------
    //MARK: - Constructors
    //---------------------------------------------------------------------------------------

    init {
        this.folders = ArrayList<Folder>()

    }

    //---------------------------------------------------------------------------------------
    //MARK: - Public Methods
    //---------------------------------------------------------------------------------------

    fun folderWithId(folderId: String): Folder? {
        return folders.find { it.folderId == folderId }
    }

    fun addFolder(folder: Folder) {
        folders.add(folder)
    }

    override fun count(): Int {
        return folders.size
    }


}