////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package com.saasplications.genie.domain.model

import org.jetbrains.spek.api.Spek
import kotlin.test.assertEquals


class DataTypeSpec : Spek({

    describe("DataType from String") {

        it("should return DataTypeNull") {
            assertEquals(DataType.NULL, DataType.fromString(null))
        }

        it("should return DataTypeString") {
            assertEquals(DataType.STRING, DataType.fromString("string"))
            assertEquals(DataType.STRING, DataType.fromString("String"))
            assertEquals(DataType.STRING, DataType.fromString("STRING"))
            assertEquals(DataType.STRING, DataType.fromString("sTrInG"))
        }

        it("should return DataTypeString") {
            assertEquals(DataType.NUMBER, DataType.fromString("number"))
            assertEquals(DataType.NUMBER, DataType.fromString("Number"))
            assertEquals(DataType.NUMBER, DataType.fromString("NUMBER"))
            assertEquals(DataType.NUMBER, DataType.fromString("nUmBeR"))
        }

        it("should return DataTypeImage") {
            assertEquals(DataType.IMAGE, DataType.fromString("blob"))
            assertEquals(DataType.IMAGE, DataType.fromString("Blob"))
            assertEquals(DataType.IMAGE, DataType.fromString("BLOB"))
            assertEquals(DataType.IMAGE, DataType.fromString("bLoB"))
        }

        it("should return DataTypeBool") {
            assertEquals(DataType.BOOL, DataType.fromString("bool"))
            assertEquals(DataType.BOOL, DataType.fromString("Bool"))
            assertEquals(DataType.BOOL, DataType.fromString("BOOL"))
            assertEquals(DataType.BOOL, DataType.fromString("boOl"))
        }

        it("should return DataTypeDate") {
            assertEquals(DataType.DATE, DataType.fromString("date"))
            assertEquals(DataType.DATE, DataType.fromString("Date"))
            assertEquals(DataType.DATE, DataType.fromString("DATE"))
            assertEquals(DataType.DATE, DataType.fromString("dAtE"))
        }

        it("should return DataTypeDateTime") {
            assertEquals(DataType.DATETIME, DataType.fromString("datetime"))
            assertEquals(DataType.DATETIME, DataType.fromString("DateTime"))
            assertEquals(DataType.DATETIME, DataType.fromString("DATETIME"))
            assertEquals(DataType.DATETIME, DataType.fromString("dAtEtImE"))
        }

    }


})
