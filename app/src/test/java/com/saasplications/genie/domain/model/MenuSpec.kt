////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package com.saasplications.genie.domain.model

import org.jetbrains.spek.api.Spek
import kotlin.test.assertEquals
import kotlin.test.assertNull

class MenuSpec : Spek({

    describe("Object creation") {
        it("should allow initialization with workFlowTrays and canChangeCompanyRoles boolean flags") {
            val menu = Menu(hasWorkflowTrays = true, canChangeCompanyRole = false)
            assertEquals(true, menu.hasWorkflowTrays)
            assertEquals(false, menu.canChangeCompanyRole)
        }
    }

    describe("Process Areas") {

        var menu : Menu? = null

        beforeEach {
            menu = Menu(hasWorkflowTrays = true, canChangeCompanyRole = false)
        }

        it("should contain a processAreas property, that is initialized empty.") {
            assertEquals(0, menu!!.processAreas.size)
        }

        it("should store a process area reference via an add method.") {
            val processArea = ProcessArea(processId = "1234", title = "My Process")
            menu!!.addProcessArea(processArea)
            assertEquals(1, menu!!.processAreas.size)
        }

        it("should allow returning a process area by id") {
            val processArea = ProcessArea(processId = "1234", title = "My Process")
            menu!!.addProcessArea(processArea)

            val retrieved = menu!!.processAreaWithId("1234")
            assertEquals(processArea, retrieved)

            assertNull(menu!!.processAreaWithId("id.not.stored"))

        }

        it("should allow returning the menuItems for all process areas as a single sorted collection") {
            val processArea1 = ProcessArea(processId = "Documents", title = "Documents")

            val activity1 = MenuItem(activityId = "Customers", title = "Customers", style = "Foobar")
            val activity2 = MenuItem(activityId = "Invoices", title = "Invoices", style = "Foobar")

            processArea1.addMenuItem(activity1)
            processArea1.addMenuItem(activity2)
            menu!!.addProcessArea(processArea1)


            val processArea2 = ProcessArea(processId = "Consultants", title = "Consultants")
            val activity3 = MenuItem(activityId = "Order Tracking", title = "Order Tracking", style = "Foobar")
            val activity4 = MenuItem(activityId = "Engagements", title = "Engagements", style = "Foobar")

            processArea2.addMenuItem(activity3)
            processArea2.addMenuItem(activity4)
            menu!!.addProcessArea(processArea2)

            val processArea3 = ProcessArea(processId = "Consultants", title = "Consultants")
            menu!!.addProcessArea(processArea3)

            assertEquals(4, menu!!.allMenuItems().size)
        }
    }

    describe("User Roles. . . ") {

        var menu : Menu? = null

        beforeEach {
            menu = Menu(hasWorkflowTrays = true, canChangeCompanyRole = false)
        }

        it("should contain a userRoles property that is initialized empty.") {
            assertEquals(0, menu!!.userRoles.size)
        }

        it("should store a user role reference, via an add method.") {
            val userRole = UserRole(roleId = "123", description = "Sales Manager")
            menu!!.addUserRole(userRole)
            assertEquals(1, menu!!.userRoles.size)
        }

    }
})