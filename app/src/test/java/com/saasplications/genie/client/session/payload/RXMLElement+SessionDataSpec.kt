////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////


package com.saasplications.genie.client.session.payload

import com.saasplications.genie.domain.client.session.payload.toMenu
import com.saasplications.genie.utils.RXMLElement
import com.saasplications.genie.utils.ResourceAsString
import org.jetbrains.spek.api.Spek
import kotlin.test.assertNotNull

class RXMLElement_SessionDataSpec : Spek({

    var element : RXMLElement? = null

    beforeEach({
        val xml = ResourceAsString("SessionData.xml")
        element = RXMLElement.elementFromXMLString(xml)
    })


    describe("Object creation") {

        it("should return a menu instance for from XML data.") {
            val menu = element!!.child("ExecXResult.ESA.Menu").toMenu()
            println(menu)
            assertNotNull(menu)
        }
    }

    describe("Parsing children") {

        it("should include the style attribute on ProcessArea.Activity children, where necessary.") {
            val menu = element!!.child("ExecXResult.ESA.Menu").toMenu()
            val processArea = menu.processAreaWithId("SalesUser")
            assertNotNull(processArea)
            val item = processArea!!.menuItemWithName("ERP.Debtor")
            assertNotNull(item)
        }

    }

//    describe("Error handling", {
//        it("should throw XML validation exception if you pass it the wrong kind of element", {
//            val xmlString = ResourceAsString(this, "Dodgy.xml")
//            RXMLElement *element = [RXMLElement elementFromXMLString:xmlString encoding:NSUTF8StringEncoding];
//
//            @try {
//                [element asMenu];
//                [NSException raise:"Should have thrown exception" format:"Assertion failed"];
//            }
//            @catch (NSException *e) {
//                [[[e reason] should] equal:"Element is not a Menu."];
//            }
//
//            @try {
//                [element asProcessArea];
//                [NSException raise:"Should have thrown exception" format:"Assertion failed"];
//            }
//            @catch (NSException *e) {
//                [[[e reason] should] equal:"Element is not a ProcessArea."];
//            }
//
//            @try {
//                [element asUserRole];
//                [NSException raise:"Should have thrown exception" format:"Assertion failed"];
//            }
//            @catch (NSException *e) {
//                [[[e reason] should] equal:"Element is not a UserRole."];
//            }
//
//        });
//    });

})