////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////


package com.saasplications.genie.utils

import org.apache.commons.io.IOUtils
import java.nio.charset.Charset

fun ResourceAsString(resourcePath: String): String
{
    val classLoader = Thread.currentThread().contextClassLoader
    val inputStream = classLoader.getResourceAsStream(resourcePath)
    if (inputStream != null)
    {
        val string = IOUtils.toString(inputStream, Charset.forName("UTF-8"))
        IOUtils.closeQuietly(inputStream)
        return string
    }
    throw RuntimeException("Resource $resourcePath not found")
}