////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package com.saasplications.genie.domain.model


import java.util.ArrayList

class Query(val queryId: String)
{
    val fields: ArrayList<FieldSchema>

    init
    {
        this.fields = ArrayList<FieldSchema>()

    }

    fun addField(fieldSchema: FieldSchema)
    {
        fields.add(fieldSchema)
    }

}