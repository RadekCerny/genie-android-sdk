////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package com.saasplications.genie.client.session.payload

import com.saasplications.genie.domain.client.session.payload.SessionDataRequest
import com.saasplications.genie.utils.ResourceAsString
import org.jetbrains.spek.api.Spek
import kotlin.test.assertEquals

class SessionDataRequestSpec : Spek({

    it("should serialize itself to xml") {
        val sessionDataRequest = SessionDataRequest(sessionToken = "xx1245")
        val xml = sessionDataRequest.toXml()
        assertEquals(ResourceAsString("SessionDataRequest.xml"), xml)
    }

})