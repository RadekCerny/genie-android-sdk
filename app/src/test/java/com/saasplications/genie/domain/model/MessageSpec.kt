////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////


package com.saasplications.genie.domain.model

import org.jetbrains.spek.api.Spek
import kotlin.test.assertEquals

class MessageSpec : Spek({


    describe("object creation") {
        it("should allow initialization with messageType and message attributes") {

            val message = Message(type = MessageType.WARNING, content = "abcd is not a valid number.")
            assertEquals(MessageType.WARNING, message.type)
            assertEquals("abcd is not a valid number.", message.content)
        }
    }


})