////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package com.saasplications.genie.domain.model


class TextGridDataCell(cellId: String, val text: String) : AbstractGridDataCell(cellId)
{

    override fun toString(): String
    {
        return String.format("TextCell: text=%s", text)
    }

}