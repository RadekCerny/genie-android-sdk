////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package com.saasplications.genie.domain.model

enum class DataType
{
    NULL,
    STRING,
    NUMBER,
    IMAGE,
    BOOL,
    DATE,
    DATETIME,
    ENUM;

    companion object
    {
        fun fromString(string: kotlin.String?): DataType
        {
            return when (string?.trim()?.toLowerCase())
            {
                "string" -> DataType.STRING
                "number" -> DataType.NUMBER
                "blob" -> DataType.IMAGE
                "bool" -> DataType.BOOL
                "date" -> DataType.DATE
                "datetime" -> DataType.DATETIME
                "enum" -> DataType.ENUM
                else -> DataType.NULL
            }
        }
    }
}






