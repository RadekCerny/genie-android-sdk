////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package com.saasplications.genie.domain.client.session.payload


import com.saasplications.genie.domain.client.ServiceException
import com.saasplications.genie.domain.model.*
import com.saasplications.genie.utils.RXMLElement


fun RXMLElement.toSessionData(): SessionData
{
    if (!this.tag().equals("ESA"))
    {
        throw ServiceException("Element is not ESA.")
    }

    val userName = this.attribute("userName")
    val operationalMode = this.attribute("operationalMode")
    val timeZone = this.attribute("timeZone")
    val blobCacheUrl = this.attribute("blobCacheURL")
    val userCultureName = this.attribute("UserCultureName")
    val menu = this.child("Menu").toMenu()

    return SessionData(userName, operationalMode, timeZone, blobCacheUrl, userCultureName, menu)
}

fun RXMLElement.toMenu(): Menu
{
    if (!this.tag().equals("Menu"))
    {
        throw ServiceException("Element is not a Menu.")
    }

    val hasWorkFlowTrays = this.attribute("hasWorkflowTrays").toBoolean()
    val canChangeCompanyRoles = this.attribute("canChangeCompanyRole").toBoolean()

    val menu = Menu(hasWorkFlowTrays, canChangeCompanyRoles)

    this.iterate("*") { element ->
        if (element.tag().equals("ProcessArea"))
        {
            menu.addProcessArea(element.toProcessArea())
        }
        else if (element.tag().equals("Roles"))
        {
            element.iterate("*") { roleElement -> menu.addUserRole(roleElement.toUserRole()) }
        }
    }


    return menu

}

fun RXMLElement.toProcessArea(): ProcessArea
{
    if (!this.tag().equals("ProcessArea"))
    {
        throw ServiceException("Element is not a ProcessArea.")
    }
    val processArea = ProcessArea(this.attribute("id"), this.attribute("title"))
    this.iterate("*") { e -> processArea.addMenuItem(e.toMenuItem()) }
    return processArea
}

fun RXMLElement.toMenuItem(): MenuItem
{
    return MenuItem(this.attribute("name"), this.attribute("title"), this.attribute("style"))
}

fun RXMLElement.toUserRole(): UserRole
{
    if (!this.tag().equals("UserRole"))
    {
        throw ServiceException("Element is not a UserRole.")
    }
    return UserRole(this.attribute("id"), this.text())
}


