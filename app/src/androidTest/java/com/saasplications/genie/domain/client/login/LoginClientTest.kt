package com.saasplications.genie.domain.client.login

import android.app.Application
import android.test.ApplicationTestCase
import com.saasplications.genie.configuraiton.UserType
import com.saasplications.genie.domain.client.login.LoginClientDefaultImpl
import com.saasplications.genie.domain.client.login.payload.SessionRequest
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit


class LoginClientTest : ApplicationTestCase<Application>(Application::class.java)
{

    fun testLoginWithValidCredentials()
    {
        val loginSignal = CountDownLatch(1)

        val serviceUrl = "http://crp.saasplications.com/GenieService/genieservice.svc/restish/CreateSessionX"
        val loginClient = LoginClientDefaultImpl(serviceUrl)
        val sessionRequest = SessionRequest(userName = "jasper", password = "sdk",
                appSite = "SOULFRESHTEST", userType = UserType.Primary)
        loginClient.createSession(sessionRequest) { sessionContext ->
            assertNotNull(sessionContext)
            loginSignal.countDown()
        }

        loginSignal.await(7, TimeUnit.SECONDS)
    }

}