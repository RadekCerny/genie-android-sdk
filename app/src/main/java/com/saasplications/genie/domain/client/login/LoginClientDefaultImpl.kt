////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////


package com.saasplications.genie.domain.client.login

import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.httpPost
import com.saasplications.genie.domain.client.Response
import com.saasplications.genie.domain.client.ResponseStatus
import com.saasplications.genie.domain.client.ServiceException
import com.saasplications.genie.domain.client.login.payload.SessionRequest
import com.saasplications.genie.domain.client.login.payload.toSessionContext
import com.saasplications.genie.domain.model.SessionContext
import com.saasplications.genie.utils.RXMLElement
import java.nio.charset.Charset
import javax.inject.Inject


class LoginClientDefaultImpl @Inject constructor(serviceUrl: String) : LoginClient
{

    val serviceUrl: String

    //---------------------------------------------------------------------------------------
    //MARK: - Constructors
    //---------------------------------------------------------------------------------------

    init
    {
        this.serviceUrl = serviceUrl
    }

    //---------------------------------------------------------------------------------------
    //MARK: - Public Methods
    //---------------------------------------------------------------------------------------

    override fun createSession(sessionRequest: SessionRequest, completion: (Response<SessionContext>) -> Unit)
    {
        FuelManager.instance.baseHeaders = mapOf(
                "Content-Type" to "application/xml",
                "Accept" to "application/xml")

        val request = sessionRequest.toXml()
        println("XML: $request")

        serviceUrl.httpPost().body(request.toByteArray(Charset.forName("UTF-8"))).response {
            request, response, result ->
            if (response.httpStatusCode == 200)
            {
                val responseString = String(response.data)
                println("Response: $responseString")
                val element = RXMLElement.elementFromXMLString(responseString)
                parse(element, completion)
            }
            else
            {
                completion(Response(ResponseStatus.ERROR, null, response.httpResponseMessage))
            }
        }
    }

    private fun parse(element: RXMLElement, completion: (Response<SessionContext>) -> Unit)
    {
        try
        {
            val sessionContext = element.toSessionContext()
            completion(Response(ResponseStatus.SUCCESS, sessionContext, null))
        } catch (e: ServiceException)
        {
            completion(Response(ResponseStatus.ERROR, null, null))
        }
    }
}