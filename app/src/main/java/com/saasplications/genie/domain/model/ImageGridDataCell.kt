////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package com.saasplications.genie.domain.model

import com.saasplications.genie.utils.appendPathComponent
import org.apache.commons.io.IOUtils
import java.io.IOException
import java.io.InputStream
import java.net.URL
import kotlin.concurrent.thread

class ImageGridDataCell(cellId: String, val path: String) : AbstractGridDataCell(cellId)
{

    init
    {
        assert(path.length > 0)
    }

    fun loadWithBaseUrl(baseUrl: String, onCompletion: (bytes: ByteArray) -> Unit)
    {
        val url = URL(baseUrl).appendPathComponent(path)
        thread {
            var inputStream: InputStream? = null
            try
            {
                inputStream = url.openStream()
                val imageBytes = IOUtils.toByteArray(inputStream)
                onCompletion(imageBytes)
            }
            catch (e: IOException)
            {
                System.err.printf("Failed while reading bytes from %s: %s", url.toExternalForm(), e.message)
                e.printStackTrace()
                // Perform any other exception handling that's appropriate.
            }
            finally
            {
                if (inputStream != null)
                {
                    inputStream.close()
                }
            }
        }
    }

}