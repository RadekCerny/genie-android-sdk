////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package com.saasplications.genie.domain.model


enum class MessageType {
    WARNING,
    ERROR;

    companion object {
        fun fromString(string: String): MessageType {
            return when (string.trim().toLowerCase()) {
                "warning" -> MessageType.WARNING
                "error" -> MessageType.ERROR
                else -> MessageType.ERROR
            }
        }
    }

    override fun toString(): String {
        when (this) {
            WARNING -> return "Warning"
            ERROR -> return "Error"

        }
    }
}
