////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package com.saasplications.genie.domain.model

import org.jetbrains.spek.api.Spek
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue


class FieldSpec : Spek({

    var field: Field? = null

    beforeEach {
        field = Field(fieldId = "op1", nullable = false, defaultValue = null, dataType = DataType.NUMBER,
                label = "First Name", hint = "Type your name")
    }

    describe("Object creation") {

        it("should allow initialization with fieldId, nullable, defaultValue, datatype, label & hint attributes") {
            assertEquals("op1", field!!.fieldId)
            assertEquals(false, field!!.nullable)
            assertEquals(null, field!!.defaultValue)
            assertEquals(DataType.NUMBER, field!!.dataType)
            assertEquals("Type your name", field!!.hint)
            println(field.toString())
        }
    }

    describe("Setting value") {

        it("should set a dirty flag to indicate the user changed a value (not yet been received by the server) ") {
            field!!.value = "new value"
            assertTrue(field!!.isDirty)
        }

    }

    describe("synchronizing state with the server/cloud") {

        it("should include a method to set a new value, as a result of server processing a delta request") {
            field!!.synchronize("new validated value")
            assertEquals("new validated value", field!!.value)
        }

        it("should revert back to a clean state, after the server has registered the delta") {
            field!!.synchronize("new validated value")
            assertFalse(field!!.isDirty)
        }
    }


})
