////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package com.saasplications.genie.domain.model


import java.util.ArrayList

class SiteList
{

    val sites: ArrayList<AppSite>

    //---------------------------------------------------------------------------------------
    //MARK: - Constructors
    //---------------------------------------------------------------------------------------

    init
    {
        this.sites = ArrayList<AppSite>()
    }

    //---------------------------------------------------------------------------------------
    //MARK: - Public Methods
    //---------------------------------------------------------------------------------------

    fun addSite(site: AppSite)
    {
        sites.add(site)
    }

    override fun toString(): String
    {
        return String.format("SiteList: %s", sites)
    }

}