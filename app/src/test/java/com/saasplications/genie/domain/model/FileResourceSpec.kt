////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package com.saasplications.genie.domain.model

import org.jetbrains.spek.api.Spek
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

class FileResourceSpec : Spek({

    var fileResource: FileResource? = null

    beforeEach {
        fileResource = FileResource(path = "/blobs/Foobar", ext = "doc", field = "foobar")
    }

    describe("object creation") {

        it("should allow initialization with path, ext and field attributes.") {
            assertNotNull(fileResource)
            assertEquals("/blobs/Foobar", fileResource!!.path)
            assertEquals("doc", fileResource!!.ext)
            assertEquals("foobar", fileResource!!.field)
            println(fileResource.toString())
        }
    }

})



