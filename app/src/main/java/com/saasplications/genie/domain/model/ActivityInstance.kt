////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package com.saasplications.genie.domain.model


import java.util.ArrayList


class ActivityInstance(val title: String, val handle: String, val persistentId: String)
{
    val subTitle: String?
        get()
        {
            if (titleField != null)
            {
                val titleField = this.fieldWithId(titleField)
                return titleField!!.value
            }
            return null
        }


    val titleField: String? = null
    val style: String? = null
    val dataSets: ArrayList<AbstractData>
    val fields: ArrayList<Field>
    val messages: ArrayList<Message>

    //---------------------------------------------------------------------------------------
    //MARK: - Constructors
    //---------------------------------------------------------------------------------------

    init
    {
        this.fields = ArrayList<Field>()
        this.messages = ArrayList<Message>()
        this.dataSets = ArrayList()
    }

    //---------------------------------------------------------------------------------------
    //MARK: - Public Methods
    //---------------------------------------------------------------------------------------


    fun allowsMethodInvocations(): Boolean
    {
        for (field in fields)
        {
            if (field.isDirty)
            {
                return false
            }
        }
        return true
    }

    fun addField(field: Field)
    {
        field.parentActivity = this
        fields.add(field)
    }

    fun fieldWithId(fieldId: String): Field?
    {
        return fields.find { it.fieldId == fieldId }
    }

    fun addMessage(message: Message?)
    {
        if (message != null)
        {
            messages.add(message)
        }
    }

    fun addData(data: AbstractData?)
    {
        if (data != null)
        {
            dataSets.add(data)
        }
    }

    fun dataWithId(dataSetId: String): AbstractData?
    {
        return dataSets.find { it.dataId == dataSetId }
    }

    //---------------------------------------------------------------------------------------
    //MARK: - Overridden Methods
    //---------------------------------------------------------------------------------------

    override fun toString(): String
    {
        return "Activity Instance: title=$title, handle=$handle, persistentId=$persistentId"
    }


}