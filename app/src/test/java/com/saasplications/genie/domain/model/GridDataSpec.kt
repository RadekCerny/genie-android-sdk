////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package com.saasplications.genie.domain.model

import org.jetbrains.spek.api.Spek
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertNull


class GridDataSpec : Spek({

    var data: GridData? = null

    beforeEach {
        data = GridData(dataId = "customersList", source = "Customer.ListMe", query = null, contextObject = null)
    }

    describe("Object creation") {

        it("Should allow instantiation with id and source attributes.", {
            assertNotNull(data)
            assertNotNull(data!!.dataId)
            assertNotNull(data!!.source)
        })
    }

    describe("Association with columns") {

        it("should return the list of fields IDs that it contains") {
            val column = Column(id = "1", fieldId = "firstName", label = "First Name",
                    dataType = DataType.STRING, width = 70)
            data!!.addColumn(column)

            val another = Column(id = "2", fieldId = "lastName", label = "Last Name",
                    dataType = DataType.STRING, width = 70)
            data!!.addColumn(another)

            val fieldNames = data!!.fieldIdentifiers()
            println(fieldNames)
            assertNotNull(fieldNames)
            assertEquals("firstName", fieldNames[0])
            assertEquals("lastName", fieldNames[1])
        }

        it("Should hold a reference to it's columns") {
            val column = Column(id = "1", fieldId = "firstName", label = "First Name",
                    dataType = DataType.STRING, width = 70)
            data!!.addColumn(column)
            assertEquals(1, data!!.columns.size)
        }

        it("Should allow retrieving a column by id") {
            val column = Column(id = "1", fieldId = "firstName", label = "First Name",
                    dataType = DataType.STRING, width = 70)
            data!!.addColumn(column)

            val retrieved = data!!.columnWithId("1")
            assertEquals(retrieved, column)

            //Not stored.
            assertNull(data!!.columnWithId("99999"))
        }

        it("Should be able to return all columns by copy.") {
            val column = Column(id = "1", fieldId = "firstName", label = "First Name",
                    dataType = DataType.STRING, width = 70)
            data!!.addColumn(column)

            val another = Column(id = "2", fieldId = "lastName", label = "Last Name",
                    dataType = DataType.STRING, width = 70)
            data!!.addColumn(another)

            assertEquals(2, data!!.columns.size)
        }


    }

    describe("Association with rows") {

        it("Should hold a reference to it's rows") {
            val row = Row(id = "1", type = "ESA.Sales.Customer")
            data!!.addRow(row)
            assertEquals(1, data!!.rows.size)
        }

        it("Should allow retrieving a row by id") {
            val row = Row(id = "1", type = "ESA.Sales.Customer")
            data!!.addRow(row)

            val retrieved = data!!.rowWithId("1")
            assertEquals(retrieved, row)

            //Not stored
            assertNull(data!!.rowWithId("99999"))
        }

        it("Should be able to return all rows by copy, as a sorted list") {
            val row = Row(id = "2", type = "ESA.Sales.Customer")
            data!!.addRow(row)

            val another = Row(id = "1", type = "ESA.Sales.Customer")
            data!!.addRow(another)


            assertEquals(2, data!!.rows.size)

            //They've been sorted.
            assertEquals(data!!.rows[0], another)
            assertEquals(data!!.rows[1], row)
        }
    }

})
