////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package com.saasplications.genie.domain.client.session.payload

import com.saasplications.genie.domain.client.GNSerializable
import com.saasplications.genie.utils.ResourceAsString
import com.samskivert.mustache.Mustache

class SessionDataRequest(sessionToken: String) : GNSerializable
{
    var sessionToken: String? = null

    init
    {
        this.sessionToken = sessionToken

    }

    override fun toXml(): String
    {
        val data = mapOf(
                "sessionHandle" to sessionToken
        )

        val template = Mustache.compiler().compile(ResourceAsString("templates/SessionDataRequest.mustache.xml"))
        return template.execute(data)
    }


}