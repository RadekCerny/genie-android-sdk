////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////


package com.saasplications.genie.domain.client.login.payload

import com.saasplications.genie.domain.client.ServiceException
import com.saasplications.genie.domain.model.SessionContext
import com.saasplications.genie.utils.RXMLElement


@Throws(ServiceException::class)
fun RXMLElement.toSessionContext(): SessionContext
{

    if (!this.tag().equals("CreateSessionXResponse"))
    {
        throw ServiceException("Element is not a CreateSessionXResponse.")
    }

    val sessionContext: SessionContext
    val sessionToken = this.child("CreateSessionXResult")
    val error = this.child("errorMessage")
    val message = if (error.isValid) error.text() else null

    if (!sessionToken.isValid && message == null)
    {
        throw ServiceException("Received neither a session token nor error")
    }
    else if (sessionToken.isValid && sessionToken.text().isNotBlank())
    {
        sessionContext = SessionContext(sessionToken.text(), message)
    }
    else
    {
        throw when (message != null && message.isNotBlank())
        {
            true -> ServiceException(message)
            else -> ServiceException("Login failed.")
        }
    }
    return sessionContext
}