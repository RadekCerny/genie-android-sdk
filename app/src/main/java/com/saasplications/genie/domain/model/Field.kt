////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package com.saasplications.genie.domain.model

import org.slf4j.Logger
import org.slf4j.LoggerFactory

class Field(val fieldId: String,
            val nullable: Boolean,
            val defaultValue: String?,
            val dataType: DataType,
            val label: String,
            val hint: String)
{
    companion object
    {
        val log: Logger = LoggerFactory.getLogger(Field::class.java)
    }


    var parentActivity: ActivityInstance? = null

    var value: String? = null
        set(value)
        {
            log.debug("$this', did finish edit with new value '$value'")
            field = value
            isDirty = true
        }

    val disabled: Boolean = false
    var isDirty: Boolean = false
        private set

    fun synchronize(validatedValue: String)
    {
        value = validatedValue
        isDirty = false
    }

    //---------------------------------------------------------------------------------------
    //MARK: - Overridden Methods
    //---------------------------------------------------------------------------------------

    override fun toString(): String
    {
        return "Field: id='$fieldId', value='$value'"
    }


}