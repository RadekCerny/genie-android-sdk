////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package com.saasplications.genie.utils

import java.net.URL

fun URL.appendPathComponent(path: String): URL
{
    val uri = this.toURI()
    val newPath = uri.path + '/' + path
    val newUri = uri.resolve(newPath)
    return newUri.toURL()
}

fun URL.lastPathComponent(): String
{
    return this.path.lastPathComponent()
}

fun URL.extensionOrNull(): String?
{
    return this.path.extensionOrNull()
}







