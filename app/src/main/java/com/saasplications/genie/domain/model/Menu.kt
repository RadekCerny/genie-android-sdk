////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package com.saasplications.genie.domain.model

import java.util.*

class Menu(val hasWorkflowTrays: Boolean, var canChangeCompanyRole: Boolean)
{
    val processAreas: HashSet<ProcessArea>
    val userRoles: HashSet<UserRole>

    //---------------------------------------------------------------------------------------
    //MARK: - Constructors
    //---------------------------------------------------------------------------------------


    init
    {
        this.processAreas = HashSet<ProcessArea>()
        this.userRoles = HashSet<UserRole>()

    }

    //---------------------------------------------------------------------------------------
    //MARK: - Public Methods
    //---------------------------------------------------------------------------------------

    fun addProcessArea(processArea: ProcessArea)
    {
        this.processAreas.add(processArea)
    }

    fun processAreaWithId(processAreaId: String): ProcessArea?
    {
        return processAreas.find { it.processId == processAreaId }
    }

    fun addUserRole(userRole: UserRole)
    {
        userRoles.add(userRole)
    }

    fun allMenuItems(): ArrayList<MenuItem>
    {
        val allMenuItems = ArrayList<MenuItem>()
        for (processArea in this.processAreas)
        {
            allMenuItems.addAll(processArea.menuItems)
        }
        return allMenuItems
    }

    //---------------------------------------------------------------------------------------
    //MARK: - Overridden Methods
    //---------------------------------------------------------------------------------------

    override fun toString(): String
    {
        return String.format("Menu: hasWorkFlowTrays=%s, canChangeCompanyRole=%s, processAreas=%s, userRoles=%s",
                if (hasWorkflowTrays) "YES" else "NO", if (canChangeCompanyRole) "YES" else "NO", processAreas,
                userRoles)
    }


}