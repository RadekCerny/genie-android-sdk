////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package com.saasplications.genie.domain.model

import org.jetbrains.spek.api.Spek
import kotlin.test.*

class ActivityInstanceSpec : Spek({

    var instance : ActivityInstance? = null

    beforeEach { instance = ActivityInstance(title = "Calculator", handle = "12345", persistentId = "123") }

    describe("Object creation") {

        it("should allow initialization with title, handle and persistent id") {
            assertEquals(instance!!.title, "Calculator")
            assertEquals(instance!!.handle, "12345")
            assertEquals(instance!!.persistentId, "123")
        }

        it("should describe itself") {
            assertEquals(instance!!.toString(), "Activity Instance: title=Calculator, handle=12345, persistentId=123")
        }

    }

    describe("Relationship to fields") {

        it("should store a collection of field objects, initially empty") {
            assertEquals(instance!!.fields.size, 0)
        }

        it("should allow field references to be added") {
            val field = Field(fieldId = "op1", nullable = false, defaultValue = null, dataType = DataType.NUMBER,
                    label = "Operand 1", hint = "Enter a value for operand 1")
            instance!!.addField(field)
            assertEquals(instance!!.fields.size, 1)
        }

        it("should allow a stored field to be retrieved by fieldId") {
            val field = Field(fieldId = "op1", nullable = false, defaultValue = null, dataType = DataType.NUMBER,
                    label = "Operand 1", hint = "Enter a value for operand 1")
            instance!!.addField(field)
            val retrieved = instance!!.fieldWithId("op1")

            retrieved!!.value = "foobar";

            val retrievedAgain = instance!!.fieldWithId("op1")
            assertEquals("foobar", retrievedAgain!!.value)
            assertEquals(1, instance!!.fields.size)

        }
    }

    describe("Relationship to DataSets") {

        it("should hold a collection of DataSets") {
            val dataSet = GridData(dataId = "customersList", source = "ESA.Sales.Customer", query = null,
                    contextObject = null)
            instance!!.addData(dataSet)
        }

        it("should allow retrieval of stored DataSets by id") {
            val dataSet = GridData(dataId = "customersList", source = "ESA.Sales.Customer", query = null,
                    contextObject = null)
            instance!!.addData(dataSet)
            val retrieved = instance!!.dataWithId("customersList")!!
            assertEquals(retrieved, dataSet)
        }

        it("should return nil when asked for a dataset of an id not being stored") {
            assertNull(instance!!.dataWithId("not.stored"))
        }
    }

    describe("Method invocations") {

        it("should prevent method invocations, unless all of it's child fields are clean") {

            //Yes if it has no fields.
            assertTrue(instance!!.allowsMethodInvocations())

            //Yes, if it has one field, that is synced with the server.
            val field = Field(fieldId = "op1", nullable = false, defaultValue = null, dataType = DataType.NUMBER,
                    label = "Operand 1", hint = "Enter a value for operand 1")
            //Field is clean.
            assertFalse(field.isDirty)

            instance!!.addField(field)
            assertTrue(instance!!.allowsMethodInvocations())

            //No, if one ore more fields are dirty.
            val another = Field(fieldId = "op1", nullable = false, defaultValue = null, dataType = DataType.NUMBER,
                    label = "Operand 1", hint = "Enter a value for operand 1")
            //Make the field dirty.
            another.value = "234.3"
            assertTrue(another.isDirty)
            instance!!.addField(another)

            assertFalse(instance!!.allowsMethodInvocations())

        }
    }

})