////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package com.saasplications.genie.domain.model

import org.jetbrains.spek.api.Spek
import kotlin.test.assertEquals
import kotlin.test.assertNotNull


class RowSpec : Spek ({

    describe("Object creation") {
        val row = Row(id = "1", type = "ESA.Sales.Customer")
        it("should allow instantiation with rowid and type attributes", {
            assertNotNull(row)
            assertEquals("1", row.id)
            assertEquals("ESA.Sales.Customer", row.type)
        })
    }

    describe("Association with cells") {

        var row: Row? = null
        var dataSet: GridData?

        beforeEach {
            row = Row(id = "1", type = "ESA.Sales.Customer")
            val column1 = Column(id = "1", fieldId = "firstName", label = "First Name", dataType = DataType.STRING, width = 70)
            val column2 = Column(id = "2", fieldId = "address", label = "Address", dataType = DataType.STRING, width = 70)
            dataSet = GridData(dataId = "foobar", source = "zzz", query = null, contextObject = null);
            dataSet!!.addColumn(column1)
            dataSet!!.addColumn(column2)
            dataSet!!.addRow(row!!)
        }

        it("Should hold a collection of cells") {
            row!!.addCellDefinition(cellId = "1", data = "Jasper")
            assertEquals(1, row!!.cells.size)
        }

        it("should allow returning a cell by cellId") {
            row!!.addCellDefinition(cellId = "1", data = "Jasper")
            row!!.addCellDefinition(cellId = "2", data = "Metro Manila")

            val retrieved = row!!.cellWithId("1")!!
            assertEquals("1", retrieved.cellId)
        }

        it("should allow returning a cell by field name") {
            row!!.addCellDefinition(cellId = "1", data = "Jasper")
            row!!.addCellDefinition(cellId = "2", data = "Metro Manila")

            val retrieved = row!!.cellWithFieldId("firstName")!!
            assertEquals("1", retrieved.cellId)
        }

        it("Should allow returning a sorted list of all cells.") {
            row!!.addCellDefinition(cellId = "1", data = "Jasper")
            row!!.addCellDefinition(cellId = "2", data = "Metro Manila")

            val cells = row!!.cells
            assertEquals(2, cells.size)

            //Cells are sorted.
            assertEquals("1", cells[0].cellId)
            assertEquals("2", cells[1].cellId)
        }
    }

})