////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package com.saasplications.genie.domain.model


import org.apache.commons.lang.builder.EqualsBuilder
import org.apache.commons.lang.builder.HashCodeBuilder
import java.util.ArrayList
import java.util.HashMap

class Row(val id: String, val type: String)
{
    var gridData: GridData? = null
    val cells: ArrayList<AbstractGridDataCell>
        get()
        {
            buildCellsIfRequired()
            val cells = ArrayList<AbstractGridDataCell>()
            cells.addAll(textCells)
            cells.addAll(imageCells)
            return cells
        }

    private val textCells: ArrayList<TextGridDataCell>
    private val imageCells: ArrayList<ImageGridDataCell>
    private val cellDefinitions: HashMap<String, String>
    private var built = false;


    //---------------------------------------------------------------------------------------
    //MARK: - Constructors
    //---------------------------------------------------------------------------------------

    init
    {
        this.cellDefinitions = HashMap<String, String>()
        this.textCells = ArrayList<TextGridDataCell>()
        this.imageCells = ArrayList<ImageGridDataCell>()
    }

    //---------------------------------------------------------------------------------------
    //MARK: - Public Methods
    //---------------------------------------------------------------------------------------

    fun addCellDefinition(cellId: String, data: String)
    {
        cellDefinitions.put(cellId, data)
    }

    fun cellWithId(cellId: String): AbstractGridDataCell?
    {
        buildCellsIfRequired()
        return cells.find { it.cellId == cellId }
    }

    fun cellWithFieldId(fieldId: String): AbstractGridDataCell?
    {
        buildCellsIfRequired()
        val column = gridData!!.columnWithFieldId(fieldId)
        if (column != null)
        {
            return this.cellWithId(column.id)
        }
        return null
    }


    fun textCells(): ArrayList<TextGridDataCell>
    {
        this.buildCellsIfRequired()
        return textCells

    }

    fun imageCells(): ArrayList<ImageGridDataCell>
    {
        this.buildCellsIfRequired()
        return imageCells
    }

    //---------------------------------------------------------------------------------------
    //MARK: - Private Methods
    //---------------------------------------------------------------------------------------

    fun buildCellsIfRequired()
    {
        if (!built)
        {
            for (cellId in ArrayList(cellDefinitions.keys))
            {
                val column = gridData!!.columnWithId(cellId) ?: throw RuntimeException(
                        String.format("No column for cell with id: %s", cellId))

                val data = cellDefinitions[cellId]
                when (column.dataType)
                {
                    DataType.STRING,
                    DataType.NUMBER,
                    DataType.DATE,
                    DataType.DATETIME,
                    DataType.ENUM -> textCells.add(TextGridDataCell(cellId, data!!))
                    DataType.IMAGE -> imageCells.add(ImageGridDataCell(cellId, data!!))
                    DataType.BOOL ->
                    {
                        //TODO: Handle this!
                    }
                    DataType.NULL ->
                    {
                        //TODO: Handle this!
                    }
                }
            }
            built = true
        }
    }

    //---------------------------------------------------------------------------------------
    //MARK: - Overridden Methods
    //---------------------------------------------------------------------------------------

    override fun toString(): String
    {
        return "Row cells = $cells"
    }

    override fun equals(other: Any?): Boolean {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    override fun hashCode(): Int {
        return HashCodeBuilder.reflectionHashCode(this)
    }
}