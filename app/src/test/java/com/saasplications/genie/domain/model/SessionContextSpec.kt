////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package com.saasplications.genie.domain.model

import org.jetbrains.spek.api.Spek
import kotlin.test.assertEquals

class SessionContextSpec : Spek({

    describe("Object creation") {

        var sessionContext: SessionContext? = null

        beforeEach {
            sessionContext = SessionContext (sessionToken = "xxlkj3249")
        }

        it("should allow initialization with sessionToken, warning, error and message.") {
            assertEquals("xxlkj3249",  sessionContext!!.sessionToken)
        }


    }

})