////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////


package com.saasplications.genie.domain.client.session

import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.httpPost
import com.saasplications.genie.domain.client.Response
import com.saasplications.genie.domain.client.ResponseStatus
import com.saasplications.genie.domain.client.ServiceException
import com.saasplications.genie.domain.client.login.payload.toSessionContext
import com.saasplications.genie.domain.client.session.payload.SessionDataRequest
import com.saasplications.genie.domain.client.session.payload.toSessionData
import com.saasplications.genie.domain.model.SessionData
import com.saasplications.genie.utils.RXMLElement
import java.nio.charset.Charset
import javax.inject.Inject


class SessionDataClientDefaultImpl @Inject constructor(serviceUrl: String) : SessionDataClient
{

    val serviceUrl: String

    //---------------------------------------------------------------------------------------
    //MARK: - Constructors
    //---------------------------------------------------------------------------------------

    init
    {
        this.serviceUrl = serviceUrl
    }

    //---------------------------------------------------------------------------------------
    //MARK: - Public Methods
    //---------------------------------------------------------------------------------------

    override fun loadSessionData(sessionDataRequest: SessionDataRequest,
                                 completion: (Response<SessionData>) -> Unit)
    {
        FuelManager.instance.baseHeaders = mapOf(
                "Content-Type" to "application/xml",
                "Accept" to "application/xml")

        val requestString = sessionDataRequest.toXml()
        println(requestString)
        val requestBytes = requestString.toByteArray(Charset.forName("UTF-8"))
        println(requestBytes)
        serviceUrl.httpPost().body(requestBytes).response {
            request, response, result ->
            val responseString = String(response.data)
            if (response.httpStatusCode == 200)
            {
                println(responseString)
                val element = RXMLElement.elementFromXMLString(responseString)
                parse(element, completion)
            }
            else
            {
                throw RuntimeException(responseString)
            }
        }
    }

    private fun parse(element: RXMLElement, completion: (Response<SessionData>) -> Unit)
    {
        try
        {
            val sessionData = element.child("ExecXResult.ESA").toSessionData()
            completion(Response(ResponseStatus.SUCCESS, sessionData, null))
        } catch (e: ServiceException)
        {
            completion(Response(ResponseStatus.ERROR, null, e.message))
        }
    }
}