////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package com.saasplications.genie.domain.model

import android.util.Log

class DataBuilder(private val dataId: String)
{

    var source: String? = null
    var query: String? = null
    var contextObject: String? = null

    private var data: AbstractData? = null

    //---------------------------------------------------------------------------------------
    //MARK: - Public Methods
    //---------------------------------------------------------------------------------------

    fun build(): AbstractData?
    {
        if (this.data == null)
        {
            Log.i("StdLog", String.format("**** Warning **** data with id %s has no data, returning nil", this.dataId))
        }
        return this.data
    }

    fun addRow(row: Row)
    {
        if (this.data == null)
        {
            this.data = GridData(this.dataId, source!!, query!!, contextObject!!)
        }
        else if (this.data is TreeData)
        {
            throw RuntimeException("Can't add both GridData and TreeData attributes to the same instance.")
        }
        val gridData = this.data as GridData?
        gridData!!.addRow(row)
    }

    fun addColumn(column: Column)
    {
        if (this.data == null)
        {
            this.data = GridData(this.dataId, source!!, query!!, contextObject!!)
        }
        else if (this.data is TreeData)
        {
            throw RuntimeException("Can't add both GridData and TreeData attributes to the same instance.")
        }
        val gridData = this.data as GridData?
        gridData!!.addColumn(column)
    }

    fun addFolder(folder: Folder)
    {
        if (this.data == null)
        {
            this.data = TreeData(this.dataId)
        }
        else if (this.data is GridData)
        {
            throw RuntimeException("Can't add both GridData and TreeData attributes to the same instance.")
        }
        val treeData = this.data as TreeData?
        treeData!!.addFolder(folder)
    }
}

