////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package com.saasplications.genie.utils

import org.apache.commons.lang.StringUtils

fun String.lastPathComponent(): String
{
    return this.substring(this.lastIndexOf('/') + 1)
}

fun String.extensionOrNull(): String?
{
    val index = this.lastIndexOf('.')
    if (index != -1)
    {
        return this.substring(index + 1)
    }
    return null
}

fun String.isAscii(): Boolean
{
    return StringUtils.isAsciiPrintable(this)
}





