////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package com.saasplications.genie.domain.model


import java.util.HashSet

class ProcessArea(val processId: String, val title: String)
{

    val menuItems: HashSet<MenuItem>

    //---------------------------------------------------------------------------------------
    //MARK: - Constructors
    //---------------------------------------------------------------------------------------

    init
    {
        this.menuItems = HashSet<MenuItem>()

    }

    //---------------------------------------------------------------------------------------
    //MARK: - Public Methods
    //---------------------------------------------------------------------------------------

    fun addMenuItem(menuItem: MenuItem)
    {
        menuItems.add(menuItem)
    }

    fun menuItemWithName(activityName: String): MenuItem?
    {
        return menuItems.find { it.activityId == activityName }
    }

    //---------------------------------------------------------------------------------------
    //MARK: - Overridden Methods
    //---------------------------------------------------------------------------------------

    override fun toString(): String
    {
        return String.format("ProcessArea: id=%s, title=%s, menuItems: %s", processId, title, menuItems)
    }


}