////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package com.saasplications.genie.domain.model


class MenuItem(val activityId: String?, val title: String?, val style: String?)
{

    //---------------------------------------------------------------------------------------
    //MARK: - Constructors
    //---------------------------------------------------------------------------------------

    @JvmOverloads constructor(activityId: String, style: String? = null) : this(activityId, null, style)
    {
    }

    //---------------------------------------------------------------------------------------
    //MARK: - Overridden Methods
    //---------------------------------------------------------------------------------------

    override fun toString(): String
    {
        return String.format("MenuItem: %s:%s", activityId, style)
    }

    override fun equals(other: Any?): Boolean
    {
        if (this === other) return true
        if (other !is MenuItem) return false

        if (if (activityId != null) activityId != other.activityId else other.activityId != null) return false
        return if (style != null) style == other.style else other.style == null

    }

    override fun hashCode(): Int
    {
        var result = if (activityId != null) activityId.hashCode() else 0
        result = 31 * result + if (style != null) style.hashCode() else 0
        return result
    }

}