////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package com.saasplications.genie.domain.client.login.payload


import com.saasplications.genie.configuraiton.UserType
import com.saasplications.genie.domain.client.GNSerializable
import com.saasplications.genie.utils.ResourceAsString
import com.samskivert.mustache.Mustache

class SessionRequest(var userName: String?,
                     var password: String?,
                     var appSite: String?,
                     var userType: UserType?) : GNSerializable
{

    override fun toXml(): String
    {
        val data = mapOf(
                "userName" to userName,
                "password" to password,
                "appSite" to appSite,
                "userType" to userType.toString()
        )

        val template = Mustache.compiler().compile(ResourceAsString("templates/SessionRequest.mustache.xml"))
        return template.execute(data)
    }


}