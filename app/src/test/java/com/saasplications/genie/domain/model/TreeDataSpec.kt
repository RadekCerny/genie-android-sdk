////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package com.saasplications.genie.domain.model

import org.jetbrains.spek.api.Spek
import kotlin.test.assertEquals
import kotlin.test.assertNull


class TreeDataSpec : Spek({

    describe("Object creation") {

        it("should allow creation with dataId attribute") {
            val treeData = TreeData(dataId = "1234")
            assertEquals("1234", treeData.dataId)
        }
    }

    describe("Folders") {

        var treeData: TreeData? = null

        beforeEach {
            treeData = TreeData(dataId = "1234")
        }

        it("should start with an empty collection of folders") {
            assertEquals(0, treeData!!.folders.size)
        }

        it("should allow adding folders") {
            val folder = Folder(folderId = "1234", title = "Tree Data", hint = "Tree Data",
                    buttonTitle = "tap me", sequence = "2")
            treeData!!.addFolder(folder)
            assertEquals(1, treeData!!.folders.size)

            val another = Folder(folderId = "1234", title = "Tree Data", hint = "Tree Data",
                    buttonTitle = "tap me", sequence = "3")
            treeData!!.addFolder(another)


            assertEquals(2, treeData!!.folders.size)
        }

        it("should sort folders according to sequence attribute") {
            val folder = Folder(folderId = "1234", title = "Tree Data", hint = "Tree Data",
                    buttonTitle = "tap me", sequence = "2")
            treeData!!.addFolder(folder)

            val another = Folder(folderId = "1234", title = "Tree Data", hint = "Tree Data",
                    buttonTitle = "tap me", sequence = "1")
            treeData!!.addFolder(another)

            assertEquals(another, treeData!!.folders[0])
            assertEquals(folder, treeData!!.folders[1])

        }

        it("should provide an accessor to return a folder by id") {
            val folder = Folder(folderId = "1234", title = "Tree Data", hint = "Tree Data",
                    buttonTitle = "tap me", sequence = "2")
            treeData!!.addFolder(folder)

            assertEquals(folder, treeData!!.folderWithId("1234"))
            assertNull(treeData!!.folderWithId("5678"))

        }

    }

})