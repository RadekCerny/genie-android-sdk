////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package com.saasplications.genie.configuraiton


import com.saasplications.genie.domain.model.SessionContext
import com.saasplications.genie.domain.model.SessionData

class SessionContextHolder {

    var context: SessionContext? = null
    var data: SessionData? = null

    fun clear() {
        this.context = null
        this.data = null
    }

}