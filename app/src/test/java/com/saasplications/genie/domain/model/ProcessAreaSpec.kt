////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////


package com.saasplications.genie.domain.model

import org.jetbrains.spek.api.Spek
import kotlin.test.assertEquals

class ProcessAreaSpec : Spek({

    describe("Object initialization") {

        it("should allow initialization with process id and title attributes.") {
            val processArea = ProcessArea(processId = "Calculator", title = "Calculator")
            assertEquals("Calculator", processArea.processId)
            assertEquals("Calculator", processArea.title)
        }
    }

    describe("Activities") {

        var processArea: ProcessArea? = null

        beforeEach({
            processArea = ProcessArea(processId = "123", title = "My Process")
        })

        it("should contain an menuItems property, that is initialized empty. ") {
            assertEquals(0, processArea!!.menuItems.size)
        }

        it("should store references to associated activities via an addActivity method") {
            val menuItem = MenuItem(activityId = "WidgetOrder", title = "Order a Widget", style = "Lead")
            processArea!!.addMenuItem(menuItem)
            assertEquals(1, processArea!!.menuItems.size)
        }

        it("should allow retrieving an activity by name.") {
            val menuItem = MenuItem(activityId = "WidgetOrder", title = "Order a Widget", style = "Lead")
            processArea!!.addMenuItem(menuItem)

            assertEquals(menuItem, processArea!!.menuItemWithName("WidgetOrder"))
        }

    }
})