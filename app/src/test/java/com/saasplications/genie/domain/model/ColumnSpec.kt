////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package com.saasplications.genie.domain.model

import org.jetbrains.spek.api.Spek
import kotlin.test.assertEquals
import kotlin.test.assertNotNull


class ColumnSpec : Spek({

    var column: Column? = null

    beforeEach {
        column = Column(id = "1", fieldId = "firstName", label = "First Name", dataType = DataType.STRING,
                width = 70)
    }

    describe("Object creation") {

        it("should allow creation with firstName, label, dataType & width attributes") {
            assertNotNull(column)
            assertEquals("firstName", column!!.fieldId)
            assertEquals("First Name", column!!.label)
            assertEquals(DataType.STRING, column!!.dataType)
            assertEquals(70, column!!.width)
        }
    }
})
