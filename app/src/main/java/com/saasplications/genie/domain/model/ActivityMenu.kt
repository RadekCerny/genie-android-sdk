////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package com.saasplications.genie.domain.model

import java.util.ArrayList

class ActivityMenu
{

    val activities: ArrayList<MenuItem>

    //---------------------------------------------------------------------------------------
    //MARK: - Constructors
    //---------------------------------------------------------------------------------------

    init
    {
        this.activities = ArrayList<MenuItem>()
    }

    //---------------------------------------------------------------------------------------
    //MARK: - Public Methods
    //---------------------------------------------------------------------------------------

    fun addMenuItem(menuItem: MenuItem)
    {
        activities.add(menuItem)
    }


}