////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////


package com.saasplications.genie.domain.model

import org.jetbrains.spek.api.Spek
import kotlin.test.assertEquals


class ResourceCollectionSpec : Spek({

    var resourceCollection : ResourceCollection? = null

    beforeEach {
        resourceCollection = ResourceCollection(title = "Documents")
    }


    describe("File resources") {

        it("should allow adding file resources", {
            val fileResource = FileResource(path = "/blobs/Foobar", ext = "pdf", field = "field.foobar")
            resourceCollection!!.addFileResource(fileResource)
            assertEquals(1, resourceCollection!!.fileResources.size)
        })

    }
})