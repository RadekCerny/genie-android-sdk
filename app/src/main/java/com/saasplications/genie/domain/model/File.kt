////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package com.saasplications.genie.domain.model

class File(val fileId: String,
           val title: String,
           val hint: String,
           val fileName: String,
           val sequence: String,
           val type: String,
           val field: String)
{

    //---------------------------------------------------------------------------------------
    //MARK: - Overridden Methods
    //---------------------------------------------------------------------------------------

    override fun toString(): String
    {
        return "File: fileId=$fileId, title=$title, hint=$hint, fileName=$fileName, sequence=$sequence, type=$type, field=$field"
    }


}