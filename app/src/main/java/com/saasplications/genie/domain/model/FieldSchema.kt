////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package com.saasplications.genie.domain.model

class FieldSchema(val name: String, val type: String, val label: String)
{

    //---------------------------------------------------------------------------------------
    //MARK: - Overridden Methods
    //---------------------------------------------------------------------------------------

    override fun toString(): String
    {
        return "FieldSchema: name=$name, type=$type, label=$label"
    }


}