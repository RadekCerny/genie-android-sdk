////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////


package com.saasplications.genie.domain.client

class Response<T> constructor(status: ResponseStatus, result: T?, message: String?)
{

    val status: ResponseStatus
    val result: T?
    val message: String?

    init
    {
        this.status = status
        this.result = result
        this.message = message
    }

    override fun toString(): String
    {
        return when (status)
        {
            ResponseStatus.ERROR -> "Error: $message"
            ResponseStatus.SUCCESS -> "Success: $result"
        }
    }

}