////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package com.saasplications.genie.domain.model

import java.util.ArrayList

class Folder(val folderId: String,
             val title: String,
             val hint: String,
             val buttonTitle: String,
             val sequence: String)
{
    val files: ArrayList<File>

    init
    {
        this.files = ArrayList<File>()
    }

    fun addFile(file: File)
    {
        this.files.add(file)
    }

}