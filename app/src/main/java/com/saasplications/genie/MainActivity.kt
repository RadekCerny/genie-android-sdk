package com.saasplications.genie

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.saasplications.genie.configuraiton.UserType
import com.saasplications.genie.domain.client.ResponseStatus
import com.saasplications.genie.domain.client.login.LoginClientDefaultImpl
import com.saasplications.genie.domain.client.login.payload.SessionRequest
import com.saasplications.genie.domain.client.session.SessionDataClientDefaultImpl
import com.saasplications.genie.domain.client.session.payload.SessionDataRequest

class MainActivity : AppCompatActivity()
{

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val loginClient = LoginClientDefaultImpl("http://crp.saasplications.com/GenieService/genieservice.svc/restish/CreateSessionX")
        val sessionDataClient = SessionDataClientDefaultImpl("http://crp.saasplications.com/GenieService/genieservice.svc/restish/ExecX")

        val sessionRequest = SessionRequest("jasper", "sdk", "SOULFRESHTEST", UserType.Primary)
        loginClient.createSession(sessionRequest) {
            if (it.status == ResponseStatus.SUCCESS) {
                sessionDataClient.loadSessionData(SessionDataRequest(it.result!!.sessionToken)) {
                    println("########### Got session data $it")
                }
            }
        }
    }
}





