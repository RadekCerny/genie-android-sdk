////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////`

package com.saasplications.genie.domain.model

import org.jetbrains.spek.api.Spek
import kotlin.test.assertEquals


class ActivitySpec : Spek({

    describe("Object creation") {

        it("should allow initialization with name and title") {
            val menuItem = MenuItem(activityId = "CALC", title = "Calculator Application", style = "Lead")
            assertEquals("CALC", menuItem.activityId)
            assertEquals("Calculator Application", menuItem.title)
            println(menuItem.toString())
        }
    }
})



