////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package com.saasplications.genie.domain.model

import org.jetbrains.spek.api.Spek
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

class FileSpec : Spek({

    var file: File? = null

    beforeEach {
        file = File(fileId = "1234", title = "Foobar", hint = "Something is inside the file.", fileName = "foobar.txt",
                sequence = "1", type = "txt", field = "foobar")
    }


    describe("Object creation") {

        it("should allow initialization with fileId, title, hint, fileName, sequence, type & field parameters") {
            assertNotNull(file)
            assertEquals("1234", file!!.fileId)
            assertEquals("Foobar", file!!.title)
            assertEquals("Something is inside the file.", file!!.hint)
            assertEquals("foobar.txt", file!!.fileName)
            assertEquals("1", file!!.sequence)
            assertEquals("foobar", file!!.field)
            println(file.toString())
        }
    }

})


