////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package com.saasplications.genie.domain.model

import org.jetbrains.spek.api.Spek
import kotlin.test.assertEquals
import kotlin.test.assertNotNull


class FolderSpec : Spek({

    describe("Object creation") {

        it("should allow instantiation with folderId, title, hint, buttonTitle, sequence attributes") {
            val folder = Folder(folderId = "123", title = "Documents", hint = "Documents about stuff",
                    buttonTitle = "tap me", sequence = "1")

            assertNotNull(folder)
            assertEquals("123", folder.folderId)
            assertEquals("Documents", folder.title)
            assertEquals("Documents about stuff", folder.hint)
            assertEquals("tap me", folder.buttonTitle)
            assertEquals("1", folder.sequence)
        }
    }

    describe("Files") {

        var folder: Folder? = null

        beforeEach {
            folder = Folder(folderId = "123", title = "Documents", hint = "Documents about stuff",
                    buttonTitle = "tap me", sequence = "1")
        }

        it("should start with an empty collection of files") {
            assertEquals(0, folder!!.files.size)
        }

        it("should allow adding files") {
            val file = File(fileId = "12345", title = "The Foobar", hint = "Foobars be here.", fileName = "foobar.txt",
                    sequence = "1", type = "txt", field = "foobar")
            folder!!.addFile(file)
            assertEquals(1, folder!!.files.size)

            val another = File(fileId = "12345", title = "The Foobar", hint = "Foobars be here.", fileName = "foobar.txt",
                    sequence = "1", type = "txt", field = "foobar")
            folder!!.addFile(another)
            assertEquals(2, folder!!.files.size)

        }
    }

})
