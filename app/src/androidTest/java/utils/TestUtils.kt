package utils

fun waitFor(seconds: Int = 7, condition: () -> Boolean, thenPerformTests: () -> Unit = {})
{
    var conditionHappened = false
    for (i in 1..seconds)
    {
        conditionHappened = condition()
        if (conditionHappened)
        {
            thenPerformTests()
            break
        }
    }
    if (!conditionHappened)
    {
        Thread.sleep(200)
        throw RuntimeException("Expected condition to happen before time-out")
    }
}
