////////////////////////////////////////////////////////////////////////////////
//
//  SAASPLICATIONS
//  Copyright 2016 SAASPLICATIONS
//  All Rights Reserved.
//
//  NOTICE: Saasplications permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package com.saasplications.genie.domain.model


class SessionData(val userName: String,
                  val operationalMode: String,
                  val timeZone: String,
                  val blobCacheUrl: String,
                  val userCultureName: String,
                  val menu: Menu)